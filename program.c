#include <stdio.h>
#include <malloc.h>

struct Info{
    int data;
};

void modify(int * x){
    *x = (*x)+1000;
}



int main(){
    
    
    // struct Info elemente[50];
    // struct Info * elems = (struct Info *)malloc(sizeof(struct Info)*50);
    
    int val = 3;
    modify(&val);  
    printf("Val: %d\n", val);
    
    int x = 10;
    printf("%d\n", x);
    
    int * px = &x; // px este de tip int * ---- adresa
    
    printf("Informatie: %d\n", px);
    printf("Valoare la adresa: %d\n", *px);
    
    return 0;
}