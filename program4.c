#include <stdio.h>
#include <malloc.h>
#include <string.h>


struct User{
    char * username; // dynamic
    char * password; // dynamic
};

void displayUser(struct User u){
     printf("NUME UTILIZATOR: %s, PAROLA: %s\n", u.username, u.password);
}


int main(){
    
    struct User u1; // alocat static
    
    struct User * adresa = (struct User *) malloc(sizeof(struct User)); // dynamic -> ALOCARE
    adresa->username = (char *) malloc(sizeof(char)*64);
    adresa->password = (char *) malloc(sizeof(char)*64);
    strcpy(adresa->username, "Andy");
    strcpy(adresa->password, "a111");
    
    
    
    displayUser(*adresa);
    
    
    
    return 0;
}


