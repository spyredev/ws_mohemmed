#include <stdio.h>
#include <malloc.h>
#include <string.h>

struct Info{
    
    
};

/**
 * structure user for representing users. Try to use the Dynamic version however
 * */
struct User{
    char username[64];
    char password[64];
};

void displayArray(int * arr, int dim){
    printf("[");
    int i;
    for(i=0; i<dim; i++){
        printf("%d, ", arr[i]);
    }
    printf("]\n");
}


void displayUser(struct User u){
     printf("NUME UTILIZATOR: %s, PAROLA: %s\n", u.username, u.password);
}

int main(){
    int arr[10] = {3, 8, 10, 8, 200, 233};
    displayArray(arr, 6);
    
    char username[64] = "Jim";
    char password[64] = "1234";
    
    printf("Username: %s, password: %s\n", username, password);

    struct User u1;
    strcpy(u1.username, "Al113");
    strcpy(u1.password, "al1");
    
    // printf("Username: %s, password: %s\n", u1.username, u1.password);
    displayUser(u1);
    
    struct User w2;
    strcpy(w2.username, "SomeUser");
    strcpy(w2.password, "abc123xyz");
    
    displayUser(w2);
    
    struct User * userDinamic = (struct User *) malloc(sizeof(struct User)*3);
    // struct User * altUserDinamic = &u1;
   
    strcpy( (*userDinamic).username, "Dude");
    strcpy( (*userDinamic).password, "test123");
    displayUser(*userDinamic);
    
    printf("Numele utilizatorului este: %s\n", (*userDinamic).username);
    printf("Parola utilizatorului este: %s\n", userDinamic->password);
    
    return 0;
}