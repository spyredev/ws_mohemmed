#include <stdio.h>
#include <malloc.h>

void modify(int x){
    x = x+10;
}

void modifyArray(int arr[], int dim){
    int i;
    for(i=0; i<dim; i++){
        arr[i] = arr[i] + 1;
    }
}


void displayArray(int * arr, int dim){
    printf("[");
    int i;
    for(i=0; i<dim; i++){
        printf("%d, ", arr[i]);
    }
    printf("]\n");
}

int main(){
    
    int valoare = 3;
    modify(valoare);
    printf("%d\n", valoare);
    
    int grades[3];
    grades[0] = 5;
    grades[1] = 7;
    grades[2] = 9;
    
    displayArray(grades, 3);
    modifyArray(grades, 3);
    displayArray(grades, 3);
    
    printf("Info: %d\n", grades);
    
    int * pAdr = grades;
    printf("ELEMENT: %d\n", *pAdr); // *pAdr - 6, pAdr - 31209879821
    printf("AL DOILEA ELEMENT: %d\n", *(pAdr+1));
    
    
    
    return 0;
}